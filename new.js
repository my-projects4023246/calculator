
const app = Vue.createApp({
    data(){
        return{
            // name: "Muhammafd Al-amin Abubakar"
            // result:0,
            first_number:"",
            second_number:"",
            operator:"",
            display:"",
            memory:"",
            click:0,
            pat:""
            // error:""
            
        }
    },
    methods:{
        // number1(){
        //     this.display = this.display + Number(1);
        // },
        // number2(){
        //     this.display = this.display + Number(2);
        // },
        // number3(){
        //     this.display = this.display + Number(3);
        // },
        // number4(){
        //     this.display = this.display + Number(4);
        // },
        // number5(){
        //     this.display = this.display + Number(5);
        // },
        // number6(){
        //     this.display = this.display + Number(6);
        // },
        // number7(){
        //     this.display = this.display + Number(7);
        // },
        // number8(){
        //     this.display = this.display + Number(8);
        // },
        // number9(){
        //     this.display = this.display + Number(9);
        // },
        number0(){
            if (this.click == 0) {
                this.display
            }
            else if (this.click >= 1) {
                this.display = this.display + Number(0);
                this.click++;
            }            
        },
        dot(){
            if (this.click == 0) {
                this.display = Number(0) + '.';
            }
            else if (this.click >= 1) {
                this.display = this.display + '.';
                this.click = "x";
            }
            else if (this.click == "x") {
                this.display 
                this.click = 1;
            }
            this.click++;
            
        },
        DisNumber(num){
            this.display = this.display + Number(num);
            this.click++;
        },
        add(num1){
            this.first_number = num1;
            this.operator = '+'; 
            this.display = "";
        },
        sub(num1){
            this.first_number = num1;
            this.operator = '-'; 
            this.display = "";
        },
        mul(num1){
            this.first_number = num1;
            this.operator = '*';
            this.display = "";
        },
        div(num1){
            this.first_number = num1;
            this.operator = '/';
            this.display = "";
        },
        clear(){
            this.display = "";
            this.first_number = "";
            this.operator = "";
            this.click = 0;
        },
        back(){
            // this.display = this.display.slice(0, -1);
            this.pat = this.display % 10;
            this.display = parseFloat(this.pat).toFixed();
        },
        memory_store(){
            this.memory = this.display;
        },
        memory_recall(){
            this.display = this.memory;
        },
        memory_plus(){
            this.memory = this.memory + this.display;
        },
        memory_minus(){
            this.memory = this.memory - this.display;
        },
        memory_clear(){
            this.memory = "";
        },
        equality(num2){
            this.second_number = num2;
            if (this.operator == '+') {
                this.display = Number(this.first_number) + Number(this.second_number); 
            }
            else if (this.operator == '-') {
                this.display = Number(this.first_number) - Number(this.second_number);
            }
            else if (this.operator == '*') {
                this.display = Number(this.first_number) * Number(this.second_number);
            }
            else if (this.operator == '/') {
                if (this.first_number == 0) {
                    this.display = "Error";
                }
                else {
                    this.display = Number(this.first_number) / Number(this.second_number);
                }
            }
            this.click = 0;
        }
        
    }
})
app.mount("#app")