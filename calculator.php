<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap/bootstap.css">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
    
    <label for="">Calculator </label><br/><br/>

    <div class="container" >
        
        <div class= "alert" role="alert" id="question"> 

            <div class="row">
                <input type="number" name="result" id="col3">
            </div>
            <div class="row">
                <div id="col" class="col-sm-3"> <a class="btn btn-secondary" href="?num=7">7</a></div>
                <div id="col" class="col-sm-3"> <a class="btn btn-secondary" href="?num=8">8</a></div>
                <div id="col" class="col-sm-3"> <a class="btn btn-secondary" href="?num=9">9</a></div>
                <div id="col" class="col-sm-3"> <a class="btn btn-secondary" href="?op=+">+</a></div>
            
            </div>

            <div class="row">
                <div id="col" class="col-sm-3"> <a class="btn btn-secondary" href="?num=4">4</a></div>
                <div id="col" class="col-sm-3"> <a class="btn btn-secondary" href="?num=5">5</a></div>
                <div id="col" class="col-sm-3"> <a class="btn btn-secondary" href="?num=6">6</a></div>
                <div id="col" class="col-sm-3"> <a class="btn btn-secondary" href="?op=-">-</a></div>
                
            </div>

            <div class="row">
                <div id="col" class="col-sm-3"> <a class="btn btn-secondary" href="?num=1">1</a></div>
                <div id="col" class="col-sm-3"> <a class="btn btn-secondary" href="?num=2">2</a></div>
                <div id="col" class="col-sm-3"> <a class="btn btn-secondary" href="?num=3">3</a></div>
                <div id="col" class="col-sm-3"> <a class="btn btn-secondary" href="?op=*">*</a></div>
            </div>

            <div class="row">
                <div id="col" class="col-sm-3"> <a class="btn btn-secondary" href="?op=.">.</a></div>
                <div id="col" class="col-sm-3"> <a class="btn btn-secondary" href="?num=0">0</a></div>
                <div id="col" class="col-sm-3"> <a class="btn btn-secondary" href="?op=+/-">+/-</a></div>
                <div id="col" class="col-sm-3"> <a class="btn btn-secondary" href="?op=/">/</a></div>
            </div>
            <div class="row"> 
                <!-- <div id="col2" class="col-sm-4">  -->
                    <!-- <a class="btn btn-secondary" href="?op==">=</a> -->
                <!-- </div> -->
                <div id="col2" class="col-sm-4"> 
                    <a  class="btn btn-secondary" href="?op==">=</a>
                </div>
                <!-- <div id="col2" class="col-sm-4">  -->
                    <!-- <a class="btn btn-secondary" href="?op==">=</a> -->
                <!-- </div> -->
            </div>

        </div>
    
    </div>    
        
    
    
    
    
    

    

</body>
</html>
